# Projeto 2 Sistemas Operacionais

## Sobre o projeto

Este projeto consiste na implementação de um jogo de forca multijogador através de uma conexão TCP, usando sockets em java.
As regras do jogo são:

O servidor, ao ser lançado, inicia uma nova partida, escolhendo uma palavra a ser adivinhada de um dicionário em arquivo-texto, onde cada palavra (ou expressão) está em uma linha própria.

Quando o cliente se conecta ao servidor, ele informa o nome do jogador para ser adicionado ao ranking do jogo. O servidor responde com **o estado atual do jogo**, dado pela _quantidade de letras da expressão_, as _letras já reveladas bem como suas posições na expressão_, e as _letras já tentadas pelos jogadores_.

O jogo acontece em tempo real, **não há turnos** para cada jogador. O primeiro que escolher uma letra certa, ganha 1 ponto por letra revelada na expressão. Se o jogar chutar uma letra errada, perde 3 pontos. Se escolher uma letra já escolhida anteriormente (certa ou errada), perde 1 ponto. Se todos os jogadores juntos cometerem 7 erros, todos perdem 5 pontos, enquanto que se a expressão em jogo for totalmente revelada, ganham 5 pontos.

A qualquer momento, um jogador pode desconectar da partida, usando algum comando (ex. ":sair"). Ele pode também, a qualquer momento, reconectar e, ao utilizar o seu nome de jogador, ter seus pontos restaurados no ranking. Novos jogadores, ao adentrarem a partida, partem de zero pontos.

Quando uma partida acaba, imediatamente o servidor escolhe outra palavra e inicia novamente.

A cada tentativa de chute de qualquer jogador, o sistema responde a todos os jogadores com o novo estado do jogo após a tentativa, e a quantidade de pontos que o jogador do chute recebeu (ou perdeu) por ele. Cada jogador pode solicitar a exibição do ranking completo com um comando (ex, ":ranking"). Somente esse jogador terá o ranking exibido.

### Comandos:
1. sair -> sair do programa
2. ranking -> Exibe uma lista de jogadores e seus pontos.

## Executando o projeto

Arquivos .jar estão disponíveis para execução do projeto. Eles estão disponíveis junto ao código fonte na pasta 'Arquivos jar' (Também estão em out/artifacts).

Para executar os arquivos abra janelas de terminal (CLI) e navegue até a pasta onde o arquivo Server.jar está localizado em sua máquina.

Executar o arquivo do servidor usando o comando:
```bash
java -jar Server.jar
```

O terminal exibirá a mensagem "Aguardando conexão".

Em seguida, vá à pasta onde o arquivo Client.jar está em sua máquina e execute:
```bash
java -jar Client.jar
```

Caso seja bem sucedido, o terminal do servidor irá responder "Cliente conectado!" e o cliente irá sinalizar "Conectado!".

### Jogando

Caso a conexão seja bem sucedida, o cliente será requisitado a colocar um nome de usuário para ser adicionado ao ranking.

Após inserir o seu nome, o jogo responderá com o estado atual do jogo.

O jogador poderá então dar seu chute de resposta. E os jogadores receberão uma atualização do estado do jogo.

Caso o usuário digite 'ranking', o sistema responderá com o ranking dos jogadores e, em seguida o estado atual do jogo novamente.

Para desconectar basta digitar 'sair'.
