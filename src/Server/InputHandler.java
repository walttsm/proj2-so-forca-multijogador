package Server;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class InputHandler extends Thread {
    public InputHandler(Socket s, DataInputStream dis, DataOutputStream dos, Jogo jogo, String jogador) {
        this.s = s;
        this.dis = dis;
        this.dos = dos;
        this.jogo = jogo;
        this.jogador = jogador;
    }

    @Override
    public void run(){
        String letra;
        String statusJogo;
        while (true) {
            try {
                /*statusJogo = jogo.statusJogo(this.jogo.getRodada());
                //enviar informação do jogo
                pw.println(statusJogo);*/
                dos.writeUTF("\n" + jogo.statusJogo(jogo.getRodada()));
                //Recebe letra do usuario
                letra = dis.readUTF();

                //Fecha a conexão caso receba o comando
                if (letra.equals("sair")) {
                    jogo.removeJogador(this);
                    this.s.close();
                    break;
                } else if (letra.equals("ranking")) {
                    dos.writeUTF(jogo.getRanking());
                } else {
                    if (!letra.equals("")) {
                        String out = this.jogo.getRodada().updateTentativa(letra.charAt(0), this);
                        dos.writeUTF(out);
                        if (jogo.getRodada().toString(jogo.getRodada().getTentativa()).equals(jogo.getRodada().toString(jogo.getRodada().getPalavraEscolhida()))) {
                            dos.writeUTF("Todos os jogadores ganharam 5 pontos pois a palavra foi descoberta");
                            jogo.startJogo();
                        }
                    }
                }


            } catch (IOException e) {
                System.out.println(e.getMessage());
                break;
            }
        }
    }

    public String getJogador() {
        return this.jogador;
    }



    final Socket s;
    final DataInputStream dis;
    final DataOutputStream dos;
    private final Jogo jogo;
    private final String jogador;
}
