package Server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main (String[] args) throws IOException {
        ServerSocket socket = new ServerSocket(10000);

        Jogo jogo = new Jogo();
        //Para continuar aceitando requests
        while (true) {
            try {
                System.out.println("Aguardando conexão...");
                Socket s = socket.accept();

                System.out.println("Cliente conectado!");
                DataInputStream dis = new DataInputStream(s.getInputStream());
                DataOutputStream dos = new DataOutputStream(s.getOutputStream());

                dos.writeUTF("Digite o seu apelido para o ranking");

                String userName = dis.readUTF();

                InputHandler IH = new InputHandler(s, dis, dos, jogo, userName);
                jogo.addJogador(IH);
                IH.start();

            } catch (Exception e) {
                System.out.println(e.getMessage());
                socket.close();
                break;
            }
        }
    }
}
