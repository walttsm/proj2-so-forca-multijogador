package Server;

import javax.sound.midi.Soundbank;
import java.security.KeyPair;
import java.util.*;

public class Jogo {

    public Jogo() {
        this.jogadores = new ArrayList<>();
        this.jogadorPontuacao = new HashMap<>();
        startJogo();
    }

    public void startJogo() {
        this.rodada = new Rodada(this);
        Scanner in = new Scanner(System.in);
    }

    public String statusJogo(Rodada rodada) {
        return rodada.toString(rodada.getPalavraEscolhida()) + "  Erros: " + rodada.getErros() + " Tamanho: " + rodada.getPalavraSize() +
                "\n" + "Letras tentadas: " + rodada.getLetrasUsadas() + "\n" +
                rodada.imprimeTentativa() +
                "\nDigite uma letra: ";
    }

    public Rodada getRodada() {
        return this.rodada;
    }

    public List<String> getJogadores() {
        List<String> saida = new ArrayList<>();
        for (InputHandler IH : jogadores) {
            saida.add(IH.getJogador());
        }
        return saida;
    }

    public void addJogador(InputHandler e) {
        jogadores.add(e);
        if (!jogadorPontuacao.containsKey(e.getJogador())) {
            jogadorPontuacao.put(e.getJogador(), 0);
        }
    }

    public void removeJogador(InputHandler e) {
        jogadores.remove(e);
    }

    public String getRanking() {
        StringBuilder sb = new StringBuilder();
        String jogador;
        for (Map.Entry kv : jogadorPontuacao.entrySet()) {
            sb.append(kv.getKey()).append(" ").append(kv.getValue()).append("\n");
        }
        return sb.toString();
    }

    public String updatePoints(int qtde, InputHandler jogador) {
        String out = "Estou recebendo valor " + qtde + " e jogador " + jogador.getJogador();
        if (qtde == -5) {
            for (InputHandler jog : jogadores) {
                jogadorPontuacao.put(jog.getJogador(), jogadorPontuacao.get(jog.getJogador()) - 5);
                out = "Todos os jogadores perderam 5 pontos pois alcançaram 7 erros";
            }
        } else {
            Integer pontuacao = jogadorPontuacao.get(jogador.getJogador());
            pontuacao += qtde;
            jogadorPontuacao.put(jogador.getJogador(), pontuacao);
            out = (qtde > 0)
                ? "O jogador " + jogador.getJogador() + " ganhou " + qtde + " pontos."
                : "O jogador " + jogador.getJogador() + " perdeu " + (qtde * -1) + " pontos";
        }
        return out;
    }

    private Rodada rodada;
    private final List<InputHandler> jogadores;
    private final HashMap<String, Integer> jogadorPontuacao;
}
