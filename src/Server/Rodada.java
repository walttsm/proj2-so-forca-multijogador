package Server;

import java.io.*;
import java.util.*;

public class Rodada {

    public Rodada(Jogo jogo){
        this.palavraEscolhida = escolherPalavra();
        this.tentativa = createTentativa(palavraEscolhida);
        this.erros = 0;
        this.letrasUsadas = new ArrayList<>();
        this.jogo = jogo;
    }

    public int getPalavraSize() {
        return this.palavraEscolhida.length;
    }

    private Character[] createTentativa(Character[] palavraEscolhida) {
        Character[] saida = new Character[palavraEscolhida.length];
        for (int i = 0; i < palavraEscolhida.length; i++) {
            saida[i] = '_';
        }
        return saida;
    }

    public Character[] getTentativa() {
        return this.tentativa;
    }

    public String toString(Character[] palavra) {
        StringBuilder sb = new StringBuilder(palavra.length);
        for (Character c : palavra) {
            sb.append(c);
        }
        return sb.toString();
    }

    public synchronized String updateTentativa(Character c, InputHandler jogador) {
        List <Character> gabarito = Arrays.asList(this.palavraEscolhida);
        String out = "";
        if (!gabarito.contains(c) || this.letrasUsadas.contains(c)) {
            if (this.letrasUsadas.contains(c)) {
                this.erros++;
                out = jogo.updatePoints(-1, jogador);
            } else {
                if (!gabarito.contains(c)) {
                    this.erros++;
                    out = jogo.updatePoints(-3, jogador);
                }
            }
            if (erros == 7) {
                out = jogo.updatePoints(-5, jogador);
                erros = 0;
            }

            addLetraUsada(c);
            return out;
        }

        int points = 0;
        for (int i = 0; i< gabarito.size(); i++) {
            if (gabarito.get(i) == c) {
                this.tentativa[i] = c;
                this.erros = 0;
                points = points + 1;
            }
        }
        out = jogo.updatePoints(points, jogador);
        addLetraUsada(c);
        return out;
    }

    public String imprimeTentativa() {
        StringBuilder sb = new StringBuilder();
        for (Character c : this.tentativa) {
            sb.append(c).append(" ");
        }
        return sb.toString();
    }

    public int getErros() {
        return erros;
    }

    public Character[] getPalavraEscolhida() {
        return palavraEscolhida;
    }

    private Character[] escolherPalavra() {
        List<String> palavras = new ArrayList<>();
        char[] palavra;
        try {
            InputStream in = getClass().getResourceAsStream("palavras.txt");
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                palavras.add(line);
            }
        } catch (IOException e) {
            System.out.println("Erro ao ler arquivo de dicionário!");
        }
        Random rand = new Random();
        palavra = palavras.get(rand.nextInt(palavras.size())).toLowerCase().toCharArray();
        Character[] saida = new Character[palavra.length];
        for (int i = 0; i < saida.length; i++) {
            saida[i] = palavra[i];
        }
        return saida;
    }

    public List<Character> getLetrasUsadas() {
        return letrasUsadas;
    }

    private void addLetraUsada(Character letra) {
        if (!this.letrasUsadas.contains(letra)) {
            this.letrasUsadas.add(letra);
        }
    }

    private  Character[] palavraEscolhida;
    private Character[] tentativa;
    private int erros;
    private List<Character> letrasUsadas;
    private Jogo jogo;

}
