package Client;

import java.util.Scanner;

public class TimedScanner implements Runnable{
    public TimedScanner(Scanner scanner) {
        this.scanner = scanner;
        buffer = new StringBuilder();
        reading = false;
        t = new Thread(this);
        t.setDaemon(true);
        t.start();
    }

    public String nextLine(long delay) {
        reading = true;
        String out = null;
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < delay && out == null) {
            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
            }
            synchronized (buffer) {
                if (buffer.length() > 0) {
                    Scanner temp = new Scanner(buffer.toString());
                    out = temp.next().trim();
                }
            }
        }
        reading = false;
        return out;
    }

    @Override
    public void run() {
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            synchronized (buffer) {
                if (reading) {
                    buffer.append(line);
                } else {
                    if (buffer.length() != 0) {
                        buffer.delete(0, buffer.length());
                    }
                }
            }
        }
    }

    public void clearBuffer(){
        buffer = new StringBuilder();
    }

    private final Scanner scanner;
    private StringBuilder buffer;
    private boolean reading;
    private Thread t;


}
