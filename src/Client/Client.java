package Client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main (String[] args) {
        try {
            Scanner scan = new Scanner(System.in);
            // Pegando endereço ip local
            InetAddress ipLocal = InetAddress.getByName("localhost");
            //Conectando no socket
            Socket socket = new Socket(ipLocal, 10000);
            System.out.println("Conectado!");

            //Criando objeto de entrada e saída de informações
            DataOutputStream serverWriter = new DataOutputStream(socket.getOutputStream());
            DataInputStream serverInput = new DataInputStream(socket.getInputStream());

            String nameQuery = serverInput.readUTF();

            System.out.println(nameQuery);

            String userName = scan.nextLine();
            serverWriter.writeUTF(userName);

            TimedScanner timedScan = new TimedScanner(scan);
            while (true) {
                // Lê status do jogo
                String response = serverInput.readUTF();

                System.out.println(response);

                if (response.contains("Digite")) {
                    String letter;
                    letter = timedScan.nextLine(6000);
                    timedScan.clearBuffer();
                    if (letter == null) {
                        letter = "";
                    }

                    if (!letter.equals("ok")) {
                        serverWriter.writeUTF(letter);
                    }

                    if (letter.equals("sair")) {
                        serverInput.close();
                        serverWriter.close();
                        socket.close();
                        break;
                    }
                }
            }
        }catch (IOException e) {
            System.out.println("Erro de comunicação");
        }

    }
}
